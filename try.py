from lxml import etree

import requests
# 爬取基本的豆瓣TOP250信息
# 伪装浏览器，headers可以从开发人员工具中找到User-Agent
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36'
}


def getResqutes():
    urls = [
        "https://movie.douban.com/top250?start={}".format(str(i)) for i in range(0, 250, 25)]
    for url in urls:
        data = requests.get(url, headers=headers)  # 此处是请求
        html = etree.HTML(data.text)  # 网页的解析
        count = html.xpath("// div[@class='item']")  # 这里是共有的xpath
        for info in count:
            title = info.xpath("div[2]/div[1]/a/span[1]/text()")  # 电影名称
            start = info.xpath("div[2]/div[2]/div/span[2]/text()")  # 电影星评
            detail = info.xpath("div[2]/div[2]/p[2]/span/text()")  # 电影的简介
            for title, start, detail in zip(title, start, detail):
                result = {
                    "title": title,
                    "start": start,
                    "detail": detail,
                }
                print(result)


if __name__ == '__main__':
    getResqutes()
