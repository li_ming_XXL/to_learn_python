import pymysql

db = pymysql.connect(host="localhost", user="root", password="123456",database="student")

cursor = db.cursor()

cursor.execute("drop table if exists student")

sql = """create table student (
        number int(20) not null,
        name varchar(20),
        score float)"""

cursor.execute(sql)
# 增加
cursor.execute("insert into student(number,name,score)values (20191201,'孙菲菲' , 99.0)")
# db.commit()
cursor.execute("insert into student(number,name,score)values (20191202,'老爷们' , 95.0)")
cursor.execute("insert into student(number,name,score)values (20191203,'化妆师' , 89.0)")

#删除
cursor.execute("delete from student where name = '孙菲菲'")
res = cursor.fetchall()
print(res)

#查找
cursor.execute("select * from student")
res = cursor.fetchall()
print(res)

#修改
cursor.execute("update student set score=score+10 where number = 20191203")
cursor.execute("select * from student")
res = cursor.fetchall()
print(res)


# 关闭数据库
db.close()
