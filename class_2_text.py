"""
文件名称:class_2_text
作者:李祎铭
时间:2020.3.4
"""
# 第一个 #

x = int(input("输入一个X:\n"))
E_x = ((x * 5) + 8) % 26
D_y = (21 * (E_x - 8)) % 26
if(x == D_y):
    print(str(x) + "加密为" + str(E_x))
else:
    print("你这个数大于25了")
# 第二个 #
print("要求：输入3个数a，n，q")
a, n, q = map(float, input().split())
if(q == 1):
    Sn = n * a
else:
    Sn = (a * ((q**n) - 1)) / (q - 1)
print("Sn的结果是" + str(Sn))
