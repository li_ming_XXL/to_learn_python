import requests
from bs4 import BeautifulSoup

url = 'https://www.bilibili.com/ranking'

response = requests.get(url)
html_text = response.text
soup = BeautifulSoup(html_text, 'lxml')

item = soup.findall('li', {'class': 'rank-item'})

print(len(item))
