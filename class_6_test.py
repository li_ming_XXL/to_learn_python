"""
项目名:class_6_test
姓名：李祎铭
学号：20181202
"""


def main(mon, day):
    n = ('摩羯座','水瓶座','双鱼座','白羊座','金牛座','双子座','巨蟹座','狮子座','处女座','天秤座','天蝎座','射手座','摩羯座')
    d = (20,19,21,20,21,22,23,23,23,24,23,22)
    if day < d[mon-1]:
        print (n[mon-1])
    else:
        print (n[mon])

if __name__ == '__main__':
    mon,day = map(int,input("请输入你生日的月和日\t").split())
    main(mon,day)
