"""
名称:class_10.py SQlite 基础操作
作者:20181202李祎铭
时间:2020.4.29
"""
import sqlite3 #建库建表增删改查
# 创建数据库
conn = sqlite3.connect("besti.db") #连接对象

# 创建一个cursor游标对象
cursor = conn.cursor()
# 创建一个表
cursor.execute('create table if not exists student (number int(10) primary key, name varchar(20), score float )')
# 插入记录
cursor.execute('insert into student (number ,name ,score ) values (20191201,"www",99.0)')
cursor.execute('insert into student (number ,name ,score ) values (20191202,"xxx",94.0)')
cursor.execute('insert into student (number ,name ,score ) values (20191203,"ccc",89.0)')
cursor.execute('insert into student (number ,name ,score ) values (20191204,"fff",76.0)')
#查询
cursor.execute('select * from student')
print(cursor.fetchall())
#修改
cursor.execute('update student set score = 59 where number= 20191201')
cursor.execute('select * from student')
print(cursor.fetchall())

# 删除
cursor.execute('delete from student where number = 20191202')
cursor.execute('select *from student')
print(cursor.fetchall())

# 关闭
conn.close()