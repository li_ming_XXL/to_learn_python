"""
名称：实验二·数学公式计算器
姓名：李祎铭
学号：20181202
"""
import re
import os
import pysnooper

def power(f):
    ret = re.split('\*\*', f)
    if len(ret) == 2:
        f = str(float(ret[0])**float(ret[1]))
    return f


def mul_div(f):
    ret = re.split(r'([*/])', f)
    if len(ret) == 3:
        if(ret[1] == '*'):
            f = str(float(ret[0]) * float(ret[2]))
        else:
            f = str(float(ret[0]) / float(ret[2]))
    return f


def add_sub(f):
    ret = re.split('([+-])', f)
    if(len(ret) == 3):
        if ret[1] == '+':
            f = str(float(ret[0]) + float(ret[2]))
        else:
            f = str(float(ret[0]) - float(ret[2]))
    return f


def no_bra(fa):
    while True:  # 幂运算
        ret = re.split('(\d+\.?\d*\*\*\d+\.?\d*)', fa, 1)
        if(len(ret) == 3):
            ret[1] = power(ret[1])
            fa = ret[0] + ret[1] + ret[2]
            continue
        else:
            break
    while True:  # 乘除运算
        ret = re.split('(\d+\.?\d*[*/]\d+\.?\d*)', fa, 1)
        if(len(ret) == 3):
            ret[1] = mul_div(ret[1])
            fa = ret[0] + ret[1] + ret[2]
            continue
        else:
            break
    while True:  # 加减运算
        if(fa == re.match('[-+]?\d+\.?\d*', fa)):
            break
        else:
            ret = re.split('(\d+\.?\d*)', fa, 2)
            if(len(ret) == 5):
                if(ret[0] == ret[2] or (ret[0] == '' and ret[2] == '+')):
                    fa = str(ret[0] + add_sub(ret[1] + '+' + ret[3]) + ret[4])
                else:
                    fa = str(ret[0] + add_sub(ret[1] + '-' + ret[3]) + ret[4])
                continue
            else:
                break
    return fa


def main(fa):
    while True:
        ret = re.split('\(([^()]+)\)', fa, 1)
        if (len(ret) == 3):
            ret[1] = str(no_bra(ret[1]))
            fa = ret[0] + ret[1] + ret[2]
            continue
        else:
            break
    return no_bra(fa)


def checkFalse(fa):
    list = ['+', '-', '*', '/']
    count_1 = 0
    count_2 = 0
    for i in fa:
        if(i == '('):
            count_1 += 1
        elif(i == ')'):
            count_2 += 1
        elif (i == ' '):
            print("不能包含空格")
            os._exit(-1)
    if(count_1 != count_2):
        print("括号不匹配！")
        os._exit(-1)
    for j in range(0, len(fa)-1):
        if((fa[j] in list) and (fa[j + 1] in list)):
            if((fa[j] == '*') and (fa[j + 1] == '*')):
                print("ok")
            else:
                print("数学公式不符合运算规则！")
                os._exit(-1)
    return True


if __name__ == '__main__':
    exp = input("请输入想要计算的数学公式：\n可以包含加减乘除以及幂运算，但请注意算式中不要包含空格和错误的运算规则！\n")
    if (checkFalse(exp)):
        res=main(exp)
        print(res)
