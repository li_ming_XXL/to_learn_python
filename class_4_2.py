"""
名称：第四次课作业题二
姓名：20181202李祎铭
日期：2020.3.18
"""
# 爬取豆瓣新片榜前十影片，并按照评分排序

import requests
from lxml import etree

movie={}

head = {
    "User-Agent": "Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.132 Mobile Safari/537.36"}
url = "https://movie.douban.com/chart"


def getResqutes():
    res = requests.get(url, headers=head)
    html = etree.HTML(res.text)
    list = html.xpath("//tr[@class='item']")
    for info in list:
        title = info.xpath("td[2]/div/a/text()")
        mark = info.xpath("td[2]/div/div/span[2]/text()")
        for title, mark in zip(title, mark):
            movie[title[25:-27]]=mark
    for value in sorted(movie,key=movie.__getitem__,reverse=True):

    #for(key, value) in movie.items():
        print(str(value) + ':' + str(movie[value]))


if __name__== '__main__':
    getResqutes()

"""for(key, value) in movie.items():
    print(str(key) + ':' + str(value))"""
