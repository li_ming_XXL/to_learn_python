"""
名称：第四次课作业题一
姓名：20181202李祎铭
日期：2020.3.18
"""


"""
王者荣耀游戏中有很多英雄角色，这些英雄也有类型上的区别，
分别是坦克、战士、刺客、法师、射手、辅助。
本次实践将应用Python中的列表存储不同类别的英雄，并遍历输出这些英雄
"""


dict = {
    '坦克': ["廉颇", "庄周", "刘婵", "吕布"],
    '战士': ["赵云", "墨子", "典韦", "哪吒"],
    '刺客': ["李白", "韩信", "兰陵王", "花木兰"],
    '法师': ["小乔", "妲己", "嬴政", "扁鹊"],
    '射手': ["孙尚香", "鲁班七号", "马可波罗", "狄仁杰"],
    '辅助': ["孙膑", "姜子牙", "蔡文姬", "太乙真人"],
    }


def addHero(Class, name):
    dict[Class].append(name)


def searchHero(name):
    j = 0
    for key in dict:
        for i in dict[key]:
            if(i == name):
                j = dict[key].index(i)
                return key, j
            else:
                continue


def deletHero(name):
    key, value = searchHero(name)
    del dict[key][value]


def changeHero(name, newname):
    key, value = searchHero(name)
    dict[key][value] = newname


def main():
    j = int(input("What do you want to do\n1:addHero\n2:eletHero\n3:changeHero\n"))
    if(j == 1):
        Class, name = map(str, input("Please input Class and name!\n").split())
        addHero(Class, name)
    elif(j == 2):
        name = input("Please input name!\n")
        deletHero(name)
    elif(j == 3):
        name, newname = map(str, input(
            "Please input name and newname!\n").split())
        changeHero(name, newname)
    for(key, value) in dict.items():
        print(str(key) + ':' + str(value))


if __name__ == '__main__':
    main()
