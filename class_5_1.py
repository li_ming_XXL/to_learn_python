"""
项目名称:格式化字符串实践-实时显示火车信息
姓名:李祎铭
学号:20181202
"""

"""
爬取http://search.huochepiao.com/chezhan/beijingnanban北京南站实时列车信息
"""
import requests
from lxml import etree
import re


class ticket:
    checi = ""
    chufa = ""
    daoda = ""
    ftime = ""
    dtime = ""
    time = ""
    licheng = 0

train = []
global i

head = {"User-Agent":"Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.149 Mobile Safari/537.36"}
url="http://search.huochepiao.com/chezhan/beijingnan"

res = requests.get(url, headers=head)
html = etree.HTML(res.text)
list = html.xpath("/html/body/table[4]/tr")


def search():
    i=0
    for info in list:
        #temp = ticket()
        if(i<=10):
            temp = ticket()
            temp.checi = str(info.xpath("td[1]/a/b/text()"))[2:7]
            add = str(info.xpath("td[2]/text()"))
            temp.chufa = str(re.findall(r'[\u4E00-\u9FA5]+', add))[2:-2]
            temp.daoda = str(info.xpath("td[2]/a/text()"))[2:-2]
            temp.dtime = str(info.xpath("td[4]/text()"))[2:-2]
            temp.ftime = str(info.xpath("td[5]/text()"))[2:-2]
            temp.time = str(info.xpath("td[6]/text()"))[2:-2]
            temp.licheng = str(info.xpath("td[7]/text()"))[2:-2]
            #print(temp.checi+","+temp.chufa+","+temp.daoda+","+temp.dtime+","+temp.ftime+","+temp.time+","+temp.licheng)
            train.append(temp)
            i = i+1
        else:
            break


def main():
    search()
    print("\n{:^44}".format("北京南站列车信息"))
    print("{0:^3}|{1:^10}|{2:{7}^6}|{3:{7}^4}|{4:{7}^4}|{5:{7}^5}|{6:{7}^4}".format("车次","出站","到站","开车时间","到站时间","运行时间","里程",chr(12288)))
    print("-"*67)
    for item in train:
        print("{0:{7}^4}|{1:{7}^6}|{2:{7}^6}|{3:^8}|{4:^8}|{5:^7}|{6:{7}^6}".format(item.checi,item.chufa,item.daoda,item.dtime,item.ftime,item.time,item.licheng,chr(12288)))

    n = int(input("是否需要添加信息\nYes=1\tNo=0\n"))
    if(n==1):
        tem = ticket()
        print("请依次输入:车次、出站、到站、开车时间、到站时间、运行时间、里程，以逗号隔开")
        tem.checi,tem.chufa,tem.daoda,tem.dtime,tem.ftime,tem.time,tem.licheng = input().split(',')
        train.append(tem)
        print("\n{:^44}".format("北京南站列车信息"))
        print("{0:^3}|{1:^10}|{2:{7}^6}|{3:{7}^4}|{4:{7}^4}|{5:{7}^5}|{6:{7}^4}".format("车次","出站","到站","开车时间","到站时间","运行时间","里程",chr(12288)))
        print("-"*67)
        for item in train:
            print("{0:{7}^4} {1:{7}^6} {2:{7}^6} {3:^8} {4:^8} {5:{7}^6} {6:{7}^6}".format(item.checi,item.chufa,item.daoda,item.dtime,item.ftime,item.time,item.licheng,chr(12288)))
    else:
        return 0

if __name__== '__main__':
    main()
