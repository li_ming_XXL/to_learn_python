import os
import socket
import base64
from Crypto.PublicKey import RSA
from Crypto.Cipher import PKCS1_v1_5

def client(inp):
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    s.connect(('127.0.0.1', 8001))

    Base_dir = os.path.dirname(os.path.abspath(__file__)) #获取当前目录

    while True:
        # inp = input('>>>').strip()
        cmd, path = inp.split(' ')
        if (cmd == "text") :
            path = os.path.join(Base_dir, path)
            file_name = os.path.basename(path)
            file_size = os.stat(path).st_size
            file_info = 'text|%s|%s' % (file_name, file_size)

            s.sendall(bytes(file_info, 'utf-8'))
            now = 0

            with open(path, 'rb') as fp:
                while now != file_size:
                    data = fp.read(1024)  # 单次发送大小
                    s.sendall(data)

                    now += len(data)
            res = ('\n>>> %s 上传完成' % file_name)
            return res
            # print('>>> %s 上传完成' % file_name)
            # data = s.recv(1024)
            # print('>>>' + data.decode())
        elif (cmd == "RSA" or cmd == "rsa"):
            msg = path
            # 读取文件中的公钥
            key = open('../TCP/public.pem').read()
            publickey = RSA.importKey(key)
            # 进行加密
            pk = PKCS1_v1_5.new(publickey)
            encrypt_text = pk.encrypt(msg.encode())
            # 加密通过base64进行编码
            result = base64.b64encode(encrypt_text)
            # print(result.decode())
            file_info = 'RSA|%s|%s' % (result.decode(),' ')
            s.sendall(bytes(file_info, 'utf-8'))
            data = s.recv(1024)
            res = '>>>' + data.decode()
            return res
            # print('>>>' + data.decode())


if __name__ == '__main__':
        client()

        
        
    
