import socket
import os
import time
import base64
from Crypto.PublicKey import RSA
from Crypto.Cipher import PKCS1_v1_5

def server():
    ip = ('127.0.0.1',8001)
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.bind(ip)
    s.listen()
    Base_dir = os.path.dirname(os.path.abspath(__file__))
    print("服务开启成功")

    while True:
        conn, add = s.accept()
        print("已连接")

        while True:
            data = conn.recv(1024) #接收数据大小

            cmd,file_name,file_size = str(data,'utf-8').split('|')
            # print(cmd)
            if(cmd == 'text') :
                path = os.path.join(Base_dir, 'data', file_name)
                file_size = int(file_size)
                now = 0
                with open(path, 'wb+') as fp:
                    while now != file_size:
                        data = conn.recv(1024)

                        fp.write(data)

                        now += len(data)

                        print('\r' + '[保存进度]:%s%.02f%%' % (
                        '>' * int((now / file_size) * 50), float(now / file_size) * 100), end='')
                print()
                print("%s 保存成功" % file_name)
                with open(path, 'rb') as fp:
                    print(fp.readline())

            elif(cmd == "RSA"):
                # 密文
                msg = file_name
                # print(msg)
                # base64解码
                msg = base64.b64decode(msg)
                # 获取私钥
                privatekey = open('private.pem').read()
                rsakey = RSA.importKey(privatekey)
                # 进行解密
                cipher = PKCS1_v1_5.new(rsakey)
                text = cipher.decrypt(msg, 'DecryptError')
                # 解密出来的是字节码格式，decodee转换为字符串
                print(text.decode())
                conn.sendall(("接收成功").encode())




if __name__ == '__main__':
        server()