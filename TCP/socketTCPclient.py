import socket
import base64
from Crypto.PublicKey import RSA
from Crypto.Cipher import PKCS1_v1_5
# 客户端的socket初始化
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect(('127.0.0.1', 8001)) #以元组的形式连接

with open("./TCP/test.txt", 'rb') as fp:
    data = fp.read()
    key = open('./TCP/public.pem').read()
    publickey = RSA.importKey(key)
    # 进行加密
    pk = PKCS1_v1_5.new(publickey)
    encrypt_text = pk.encrypt(data)
    # 加密通过base64进行编码
    result = base64.b64encode(encrypt_text)
    print(result.decode())
#

s.sendall(bytes(result.decode(),'utf-8')) #需要编码

#接收
data = s.recv(1024)
print(data.decode())
s.close