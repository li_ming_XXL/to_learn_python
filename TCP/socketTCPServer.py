import socket
# 服务端的socket初始化
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

s.bind(('127.0.0.1', 8001)) #绑定
s.listen() #监听

conn,address = s.accept()
data = conn.recv(1024)
print(data.decode())
conn.sendall(("服务器已经收到了数据内容："+str(data.decode())).encode())
s.close()