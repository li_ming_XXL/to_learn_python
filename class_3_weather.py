"""
名称：判断当前天气是否适合打网球
姓名：李祎铭
学号：20181202
"""
import requests
import pysnooper
import time

url = 'https://free-api.heweather.net/s6/\
weather/now?location=tianjin&key=8fdfbac26980494dabb4eb73d708df26'
res = requests.get(url).json()
result = res["HeWeather6"][0]["now"]
# print(result)
# 其实没必要这么写函数……但是这是为了可用pysnooper进行调试


class weather:
    time = 0
    cond_txt = ""
    vis = 0
    wind_spd = 0


tianjin = weather()


@pysnooper.snoop()
def Now(tianjin):
    tianjin.time = time.localtime(time.time()).tm_hour
    tianjin.cond_txt = result["cond_txt"]
    tianjin.vis = int(result["vis"])
    tianjin.wind_spd = int(result["wind_spd"])

@pysnooper.snoop()
def main(obj):
    Now(obj)
    nowTime = obj.time
    #print(obj.time)
    if(nowTime > 17):
        print("太晚了别打了")
        return 0
    else:
        if(obj.cond_txt != '晴'):
            print("天气不好，别打了")
            return 0
        else:
            if(obj.vis < 2):
                print("看不清楚，不要打了")
                return 0
            else:
                if(obj.wind_spd >= 15):
                    print("风太大了，不要打了")
                    return 0
                else:
                    print("天气不错，去打球吧！")


if __name__ == '__main__':
    main(tianjin)
