"""
名称：class_9.py彩蛋2
时间：2020.4.22
作者：20181202 李祎铭
"""
from Cryptodome.Cipher import DES
from docx import Document
docu = r"F:\NEO_Python\1812\明文.docx"


def getText(filename):
    doc = Document(filename)
    fullText = ""
    for para in doc.paragraphs:
        # print(para.text)
        fullText=fullText+para.text+'\n'
        # print(fullText)
    return fullText


key = b'abcdefgh'  # 密钥 8位或16位,必须为bytes
# def pad(text):
#     """
#     # 加密函数，如果text不是8的倍数【加密文本text必须为8的倍数！】，那就补足为8的倍数
#     :param text:
#     :return:
#     """
#     while len(text) % 8 != 0:
#         text += ' '
#     return text


des = DES.new(key, DES.MODE_ECB)  # 创建一个DES实例
text = getText(docu)
# padded_text = pad(text)

utf8_text = ""
# for item in text:
#     utf8_text += item.encode('unicode_escape').decode('utf-8')
padded_text= text + (8 - (len(text) % 8)) * ' '+2* ' '
print(len(padded_text.encode('utf-8')))
encrypted_text = des.encrypt(padded_text.encode('utf-8'))  # 加密
print(encrypted_text)
# rstrip(' ')返回从字符串末尾删除所有字符串的字符串(默认空白字符)的副本
plain_text = des.decrypt(encrypted_text).decode().rstrip(' ')  # 解密
print(plain_text)


